import Vue from 'vue'
import Vuex from 'vuex'

import phone from './modules/phone'
import contacts from './modules/contacts'
import gps from './modules/gps'
import photos from './modules/photos'
import messages from './modules/messages'
import appels from './modules/appels'
import bank from './modules/bank'
import pacific from './modules/pacific'
import notes from './modules/notes'
import bourse from './modules/bourse'
import tchat from './modules/tchat'
import twitter from './modules/twitter'
import emergencies from './modules/emergencies'
import fabbyBird from './modules/fabbyBird'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    phone,
    contacts,
    photos,
    gps,
    messages,
    appels,
    bank,
    pacific,
    bourse,
    notes,
    tchat,
    twitter,
    emergencies,
    fabbyBird
  },
  strict: true
})

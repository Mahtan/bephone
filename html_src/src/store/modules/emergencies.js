import PhoneAPI from './../../PhoneAPI'

const state = {
  emergencies: [],
  identifier: ''
}

const getters = {
  emergencies: ({ emergencies }) => emergencies,
  identifier: ({ identifier }) => identifier
}

const actions = {
  updateEmergency (context, { id, display, number }) {
    PhoneAPI.updateEmergency(id, display, number)
  },
  addEmergency (context, { display, number }) {
    PhoneAPI.addEmergency(display, number)
  },
  deleteEmergency (context, { id }) {
    PhoneAPI.deleteEmergency(id)
  },
  resetEmergency ({ commit }) {
    commit('SET_EMERGENCIES', [])
  }
}

const getSortedEmergencies = (emergencies) => {
  return emergencies.sort((a, b) => b.time - a.time).map((a) => {
    const playerName = a.player.split('- #')[0]
    const phoneNumber = a.player.split('- #')[1]
    a.playerName = playerName
    a.phoneNumber = phoneNumber

    return a
  })
}

const mutations = {
  SET_EMERGENCIES (state, emergencies) {
    state.emergencies = getSortedEmergencies(emergencies)
  },
  SET_IDENTIFIER (state, identifier) {
    state.identifier = identifier
  },
  ADD_EMERGENCY (state, emergency) {
    const newEmergencies = state.emergencies
    newEmergencies.push(emergency)
    state.emergencies = getSortedEmergencies(newEmergencies)
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}

if (process.env.NODE_ENV !== 'production') {
  // eslint-disable-next-line
  state.emergencies = [{
    id: 1,
    player: 'Robert Nichon - #555-0600',
    position: '{"z":30.0,"y":-1446.5,"x":295.8,"heading":0.0}',
    street_name: 'Street Rd',
    reason: 'noyade',
    taken_by: '1',
    taken_by_name: 'Arturo Braccheti',
    time: 1621878558,
    resolved: 0
  },
  {
    id: 2,
    player: 'Dennis Wallen - #555-6131',
    position: '{"z":30.0,"y":-1446.5,"x":295.8,"heading":0.0}',
    street_name: 'Street Rd',
    reason: "comprempa ce qui s'est passé",
    taken_by: '2',
    taken_by_name: 'Vanessa',
    time: 1621878626,
    resolved: 1
  },
  {
    id: 3,
    player: 'Sora Sora - #555-7519',
    position: '{"z":30.0,"y":-1446.5,"x":295.8,"heading":0.0}',
    street_name: 'Street Rd',
    reason: '',
    taken_by: '3',
    taken_by_name: 'Docteur Maison',
    time: 1621878626,
    resolved: 0
  },
  {
    id: 4,
    player: 'Marc Lenvi - #555-4313',
    position: '{"z":30.0,"y":-1446.5,"x":295.8,"heading":0.0}',
    street_name: 'Street Rd',
    reason: 'ye me soui pris oune poto yavé pas la ceintoure',
    taken_by: undefined,
    taken_by_name: undefined,
    time: 1621878626,
    resolved: 0
  }]
}

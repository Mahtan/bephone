import PhoneAPI from '../../PhoneAPI'

const state = {}

const getters = {}

const actions = {
  toggleGPS (context, { toggle }) {
    PhoneAPI.toggleGPS(toggle)
  }
}

const mutations = {}

export default {
  state,
  getters,
  actions,
  mutations
}
